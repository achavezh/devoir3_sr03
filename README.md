# Devoir 3 SR03 : application de banque en ligne sécurisé
## Membres:
- CARREZ Emilien
- CHÁVEZ HERREJÓN Andrea

## Tutoriel installation du git en local 
##### Pour télécharger:
Tapez les commandes successives dans le repertoire de votre choix:
1. SSH
```bash
git clone git@gitlab.utc.fr:achavezh/devoir3_sr03.git
git clone https://gitlab.utc.fr/achavezh/devoir3_sr03.git
```
```bash
cd devoir3_sr03 											#on se place dans le répertoire git
```

##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
## Tutoriel d'utilisation
##### Pour importer la base de données sur MySQL phpMyAdmin

Utilisez votre utilisateur (root) et votre mot de passe, ensuite : 

1. Créez une nouvelle base de données (nous conseillons de mettre comme nom "sr03_dm3").
2. Cliquez sur la nouvelle base donnée et allez vers l'onglet "Import".
3. Sélectionnez le fichier téléchargé d'après le répertoire.
4. Finalement cliquez sur "Go".

##### Pour faire "run" le projet :

1. Avant de tester le projet, vous devez vérifier que les informations qui contient le fichier "config.php" a les bons paramètres sinon vous devriez les changer. Le fichier est situé dans :

```bash
sr03p21 > config > config.php
```

Vous pouvez bien sur changer l'utilisateur, mot de passe et les ports. 

2. Copiez le dossier téléchargé sr0p21 dans le dossier www du Wamp situé dans :

```bash
C:\ > wamp64 > www                          #sous Windows
```

3. Après vous faire commencer les services de WampServer 

4. Tapez sur la barre de votre navigateur http://localhost/sr03p21/vw_login.php

##### Pour faire tester les différents types de profil :

La base de donnée est chargé avec des utilisateurs par défaut, vous pouvez créér autant des utilisateurs de type client, par contre il contient un utilisateur de type employé mis directement dans la base :

1. Profil employé : login : admin; mot de passe : 27127127

Si vous voulez rajouter de nouveaux employés, vous pouvez créer un client et modifier le champ 'profile_user' dans phpMyAdmin afin de le remplacer par employe. 
