<?php
  require_once('include.php');

  session_start();
  //On vérifie qu'on a recue le numero de compte et que l'utilisateur est connecté 
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == ""){
    header('Location: vw_login.php');
    exit();
  }
  else{
      if(!isset($_SESSION["expediteur"]) || $_SESSION["expediteur"] == ""){
      header('Location: vw_moncompte.php');
      exit();
    }
  }

  $mytoken = bin2hex(random_bytes(128)); // token qui va servir à prévenir des attaques CSRF
  $_SESSION["mytoken"] = $mytoken;
?>

<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <script text="text/javascript">

    tbimage=new Array(1,2,3,4,5,6,7,8,9,0)//Numéros que peut contenir le pave Virtuel

//Fonction init() : Pour initiliser le pave virtuel
    function init(){
    var tdcollection=document.getElementById('virtualPave').getElementsByTagName('td')

    for (var i = 0; i< tdcollection.length-2; i++){ // on selectionne chaque case du tableau et on l'initialise avec une valeur aléatoire du tableau tbimage

      if(tbimage.length==1){
        tdcollection[i].firstChild.nodeValue=tbimage[0]
    }
    else{
      var spl=Math.round(Math.random()*(tbimage.length-1)) // on génère une position aléatoire possible de tbimage
      tdcollection[i].firstChild.nodeValue=tbimage[spl] // on l'inscrit dans la case du tableau traité
      tbimage.splice(spl,1) // on enlèe le numéro du tableau
    }
    }

    for (i=0;i<tdcollection.length;i++){
        tdcollection[i].indice=i
        tdcollection[i].onclick=function(){if(this.indice<10){document.getElementById('pass').value+=this.innerHTML;} // si on appuie sur un nombre, il se note dans le input text 
                                                               if(this.indice==10){document.getElementById('pass').value=document.getElementById('pass').value.substr(0,document.getElementById('pass').value.length-1)} // si on appuie sur le "e" on erase un caractère
                                                               if(this.indice==11){document.getElementById('pass').value=""} //si on appuie sur "a" on efface
                                          }

        }

    }
//Fonction openForm() : Pour déplier un petit formulaire qui recevra le mot de passe pour continuer avec un virement 
    function openForm() {
      document.getElementById("popMDP").style.display = "block";
    }
//Fonction openForm() : Pour fermer le formulaire précedent 

    function closeForm(){
      document.getElementById("pass").value = "";
      document.getElementById("popMDP").style.display = "none";
    }
//Fonction handleClick(cb) : Pour changer le type de l'input qui contient le mot de passe (text vers password et à l'inverse)

    function handleClick(cb) { // permer de cacher ou montrer le mdp
    if(cb.checked)
      $('#pass').attr("type","text");
    else
      $('#pass').attr("type","password");
    }

</script>
  <title>Mes Virements</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body onload='init()'>

<div class="header">
  <h1>Site Web Securisée</h1>
  <p>Devoir 3<b> SR03</b></p>
</div>
<nav>
  <li><a href='vw_moncompte.php'>Accueil</a></li>
  <li><a href="myController.php?action=msglist">Messagerie</a></li>
  <li> <form method="POST" action="myController.php">
       <input type="hidden" name="expediteur" value="<?php echo $_SESSION["connected_user"]["numero_compte"];?>">
       <input type="hidden" name="action" value="acces_trans">
        <button class="ressemble_link">Virement</button>
        </form></li>
  <?php
    if($_SESSION["connected_user"]["profil_user"]=="EMPLOYE"){
  ?>
    <li><a href="myController.php?action=clients">Fiches Clients</a></li>
  <?php
  }
   ?>
  <li style="float:right"> <form method="POST" action="myController.php">
       <input type="hidden" name="action" value="disconnect">
        <button class="ressemble_link">Déconnexion</button>
        </form></li>
</nav>

   <div class="container">

    <div class="side">
      <h2>Bienvenue <?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?></h2><hr>
      <h3>Votre compte</h3>
      <b>N° compte : </b><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span><br><br>
      <b>Solde : </b><span><?php echo $_SESSION["connected_user"]["solde_compte"];?> &euro;</span><br><br><hr>
      <h3>Vos informations personnelles</h3>
      <b>Login : </b><span><?php echo $_SESSION["connected_user"]["login"];?></span><br><br>
      <b>Profil : </b><span><?php echo $_SESSION["connected_user"]["profil_user"];?></span><br>

    </div>

    <div class="main">
            <h2>Transferer de l'argent</h2><hr><br><br>
            <form methode="POST" action="myController.php">

              <br><b>N° compte destinataire : </b><br><br>

              <input type="text" minlength="12" maxlength="12" autocomplete="off" required name="destinataire"><br><br>
              <input type="hidden" name="expediteur" value="<?php echo $_SESSION["expediteur"];?>">

              <b>Montant à transferer : </b><br><br>

              <input type="text" maxlength="10" autocomplete="off" required name="montant"><br>
              <input type="hidden" name="action" value="transfert">
              <input type="hidden" name="mytoken" value="<?php echo $mytoken; ?>">
              <button type="button" onclick="openForm()">Transférer</button><br><br>

              <?php
              if (isset($_REQUEST["trf_ok"])) {
                echo '<p>Virement effectué avec succès.</p>';
              }
              if (isset($_REQUEST["bad_mt"])) {
                echo '<p class="errmsg">Le montant saisi est incorrect : '.htmlentities($_REQUEST["bad_mt"], ENT_QUOTES).'. avez- vous assez d\'argent sur votre compte ? </p>';
              }
              if (isset($_REQUEST["meme_Cmpt"])) {
                echo '<p class="errmsg">Problème avec la compte : '.$_REQUEST["meme_Cmpt"].'. <b> Tu ne peux pas te transférer de l\'argent à toi même </b> </p>';
              }
              if (isset($_REQUEST["bad_nCompte"])) {
                echo '<p class="errmsg">Problème avec le compte : '.$_REQUEST["bad_nCompte"].', <b>il n\'existe pas </b></p>';
              }
              if (isset($_REQUEST["bad_pwd"])) {
                echo '<p class="errmsg"> Mot de passe incorrect </p>';
              }
              if (isset($_REQUEST["err_token"])) {
                echo '<p class="errmsg">Echec virement : le contrôle d\'intégrité a échoué.</p>';
              }
              ?>
    </div>

    <div class="popup" id="popMDP">
      <div class="form-container">
        <h1>Valider la transaction</h1>
        <input type="password" minlength="8" id='pass' name='mdp'  placeholder="Mot de passe" required onkeypress="return false;"/><br/>
        <input type="checkbox" onclick="handleClick(this);"/>Montrer/Cacher la mot de passe<br><br>
        <table id='virtualPave' class="login">
        <tr>
          <td>7</td>
          <td>8</td>
          <td>9</td>
        </tr>
        <tr>
          <td>4</td>
          <td>5</td>
          <td>6</td>
        </tr>
        <tr>
          <td>1</td>
          <td>2</td>
          <td>3</td>
        </tr>
        <tr>
          <td>0</td>
          <td>e</td>
          <td>a</td>
        </tr>
      </table><br>

        <button type="submit" class="btn">Valider la transaction</button>
        <button type="button" class="btn cancel" onclick="closeForm()">Annuler</button>
      </form>
    </div>
    </div>
   </div>
    <footer>
      <p><b>Authors : </b> Andrea Chávez et Emilien Carrez <b> SR03 P21 </b</p>
    </footer>
</body>
</html>
