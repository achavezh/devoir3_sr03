<?php
  require_once('include.php');

  session_start();

  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
      // utilisateur non connecté
      header('Location: vw_login.php');
      exit();
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mes messages</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body>
<div class="header">
  <h1>Site Web Securisée</h1>
  <p>Devoir 3<b> SR03</b></p>
</div>
<nav>
  <li><a href='vw_moncompte.php'>Accueil</a></li>
  <li><a href="myController.php?action=msglist">Messagerie</a></li>
  <li> <form method="POST" action="myController.php">
       <input type="hidden" name="expediteur" value="<?php echo $_SESSION["connected_user"]["numero_compte"];?>">
       <input type="hidden" name="action" value="acces_trans">
        <button class="ressemble_link">Virement</button>
        </form></li>
  <?php
    if($_SESSION["connected_user"]["profil_user"]=="EMPLOYE"){
  ?>
    <li><a href="myController.php?action=clients">Fiches Clients</a></li>
  <?php
  }
   ?>
  <li style="float:right"> <form method="POST" action="myController.php">
       <input type="hidden" name="action" value="disconnect">
        <button class="ressemble_link">Déconnexion</button>
        </form></li>
</nav>

   <div class="container">

    <div class="side">
      <h2>Bienvenue <?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?></h2><hr>
      <h3>Votre compte</h3>
      <b>N° compte : </b><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span><br><br>
      <b>Solde : </b><span><?php echo $_SESSION["connected_user"]["solde_compte"];?> &euro;</span><br><br><hr>
      <h3>Vos informations personnelles</h3>
      <b>Login : </b><span><?php echo $_SESSION["connected_user"]["login"];?></span><br><br>
      <b>Profil : </b><span><?php echo $_SESSION["connected_user"]["profil_user"];?></span><br>

    </div>

    <div class="main">
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Messages reçus</h2>

          <div class="liste">
            <table>
              <tr><th>Expéditeur</th><th>Sujet</th><th>Message</th></tr>
              <?php
              foreach ($_SESSION['messagesRecus'] as $cle => $message) { //on parcourt les messages reçus par l'utilisateur et stockés dans la session et on les affiche
                echo '<tr>';
                echo '<td>'.$message['nom'].' '.$message['prenom'].'</td>';
                echo '<td>'.htmlentities($message['sujet_msg'], ENT_QUOTES).'</td>';
                echo '<td>'.htmlentities($message['corps_msg'], ENT_QUOTES).'</td>';
                echo '</tr>';
              }
               ?>
            </table>
          </div><br><br>
      <form action="myController.php">
        <input type='hidden' name='action' value='acces_envoie_message'>
        <button>Envoyer messages</button>
      </form>
    </div>
   </div>
   <footer>
      <p><b>Authors : </b> Andrea Chávez et Emilien Carrez <b> SR03 P21 </b</p>
    </footer>
</body>
</html>
