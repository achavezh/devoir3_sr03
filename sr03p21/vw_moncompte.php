<?php
  require_once('include.php');

  session_start();

  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
     // utilisateur non connecté
     header('Location: vw_login.php');
     exit();
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mon Compte</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body>

<div class="header">
  <h1>Site Web Securisée</h1>
  <p>Devoir 3<b> SR03</b></p>
</div>
<nav>
  <li><a href='vw_moncompte.php'>Accueil</a></li>
  <li><a href="myController.php?action=msglist">Messagerie</a></li>
  <li> <form method="POST" action="myController.php">
       <input type="hidden" name="expediteur" value="<?php echo $_SESSION["connected_user"]["numero_compte"];?>">
       <input type="hidden" name="action" value="acces_trans">
        <button class="ressemble_link">Virement</button>
        </form></li>
  <?php
    if($_SESSION["connected_user"]["profil_user"]=="EMPLOYE"){
  ?>
    <li><a href="myController.php?action=clients">Fiches Clients</a></li>
  <?php
  }
   ?>
  <li style="float:right"> <form method="POST" action="myController.php">
       <input type="hidden" name="action" value="disconnect">
        <button class="ressemble_link">Déconnexion</button>
        </form></li>
</nav>

   <div class="container">

    <div class="main">
      <h2>Bienvenue <?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?></h2><hr><br>
      <h3>Votre compte</h3>
      <b>N° compte : </b><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span><br><br>
      <b>Solde : </b><span><?php echo $_SESSION["connected_user"]["solde_compte"];?> &euro;</span><br><br><br><hr><br>
      <h3>Vos informations personnelles</h3>
      <b>Login : </b><span><?php echo $_SESSION["connected_user"]["login"];?></span><br><br>
      <b>Profil : </b><span><?php echo $_SESSION["connected_user"]["profil_user"];?></span><br>
      <br>
    </div>
   </div>
    <footer>
      <p><b>Authors : </b> Andrea Chávez et Emilien Carrez <b> SR03 P21 </b</p>
    </footer>
</body>
</html>
