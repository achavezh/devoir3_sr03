<?php
  require_once('myModel.php');
  require_once('include.php');
  require_once('reCaptcha/autoload.php');

  session_start();

  // URL de redirection par défaut (si pas d'action ou action non reconnue)
  $url_redirect = "index.php";

    if (isset($_REQUEST['action'])) {
      $car_interdits = array("'", "\"", ";","%", "<", ">","\/","\""); // une liste de caractères que je choisis d'interdire

        if ($_REQUEST['action'] == 'authenticate') {
          /* ======== AUTHENT ======== */
            if (ipIsBanned($_SERVER['REMOTE_ADDR'])){
                // Rédirection si cette IP est bloquée
                $url_redirect = "vw_login.php?ipbanned";
            }
            else if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
              // Manque du login ou mot de passe
              $url_redirect = "vw_login.php?nullvalue";

            } else if (isset($_REQUEST['g-recaptcha-response'])) {
                // On recoit le captcha 
                $recaptcha = new \ReCaptcha\ReCaptcha('6LfEmu0aAAAAAOr9pdhzi-NEv34fpeqLaMXHamzY');
                //Et on vérifie si c'est correct
                $resp = $recaptcha->verify($_REQUEST['g-recaptcha-response']);

                //Si la réponse est bonne on va chercher l'utilisateur
                if ($resp->isSuccess()) {
                  $utilisateur = findUserByLoginPwd(str_replace($car_interdits, "", $_REQUEST['login']),str_replace($car_interdits, "", $_REQUEST['mdp']),$_SERVER['REMOTE_ADDR']);

                  if ($utilisateur == false) {
                  // echec avec authentification
                    $url_redirect = "vw_login.php?badvalue";

                  } else {
                    // authentification réussie
                    $_SESSION["connected_user"] = $utilisateur;
                    // On va rédirectionner à l'accueil
                    $url_redirect = "vw_moncompte.php";
                  }
                } else {
                  // mauvais réponse pour le captcha. 
                    $url_redirect = "vw_login.php?badCaptcha";
                }
            }
            else{
              $url_redirect = "vw_login.php?forgotCaptcha";
            }

      } else if ($_REQUEST['action'] == 'disconnect') {
          /* ======== DISCONNECT ======== */
          session_unset();// On enléve la session
          $url_redirect = "vw_login.php?disconnect";

      } else if ($_REQUEST['action'] == 'acces_envoie_message') {
          /* ======== ENVOYER MESSAGES ======== */

          // On va chercher la liste de tous utilisateurs existantes dans la base de données
          $_SESSION["listeUsers"] = findAllUsers();
          // On rédirige vers la bonne page, alors celui pour envoyer messages
          $url_redirect = "vw_envoyerMessage.php";

      } else if ($_REQUEST['action'] == 'acces_trans'){
          /* ======== ENVOYER MESSAGES ======== */
          // On recoit le numero de compte de l'expéditeur
          $_SESSION["expediteur"]=$_REQUEST["expediteur"];
          // On rédirige vers la page de Virement 
          $url_redirect = "vw_effectuerVirement.php";

      } else if ($_REQUEST['action'] == 'transfert') {
          /* ======== TRANSFERT ======== */
          //On recoit et vérifie la mot de passé mis dans le pave virtuel
          if(verifyMDP($_SESSION['connected_user']['login'],$_REQUEST['mdp'])){
            $mnt = $_REQUEST['montant'];
            $cmp_dest = $_REQUEST['destinataire'];
            $cmp_exp = $_REQUEST['expediteur'];
            $solde_source = findSolde($cmp_exp); //On cherche la solde du compte qui va transferer l'argent
            if($solde_source!=false){//Si le solde_source est diffèrent de null on continue
              if(!isset($_REQUEST['mytoken']) || $_REQUEST['mytoken'] != $_SESSION['mytoken']){
                //Si le token est null ou il n'existe pas, on va rediriger à la page de Virement avec l'erreur respectif 
                $url_redirect = "vw_effectuerVirement.php?err_token";
              }
              else{
                //On vérifie que le montant envoyé soit numérique et qu'il soit suffisant pour faire le virement
                if (is_numeric ($mnt) && $mnt > 0 && ($solde_source - $mnt)>0) {
                      //On vérifie que la compte destinaire soit diffèrent du numéro de compte source
                      if($cmp_dest <> $_SESSION["connected_user"]["numero_compte"]){
                        //On vérifie que les deux numéros de compte existent
                          if(!compteExistant($cmp_dest) && !compteExistant($cmp_exp)){
                            //Une fois qu'il passe les filtres, on fait le transfert :
                              transfert($cmp_dest, $cmp_exp, $mnt);
                            // Si le numéro de compte expediteur est celui du client connecté on va rester le montant et rédiriger 
                              if($cmp_exp == $_SESSION["connected_user"]["numero_compte"])
                                $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_REQUEST['montant'];
                              $url_redirect = "vw_effectuerVirement.php?trf_ok&num_compte";
                          }else{
                            //On rédirige l'erreur si le numéro de compte est inexistant
                              $url_redirect = "vw_effectuerVirement.php?bad_nCompte";
                          }
                      }else{
                        //On rédirige l'erreur si le numéro de compte expediteur est le même que celui du client connecté 
                          $url_redirect = "vw_effectuerVirement.php?meme_Cmpt";
                      }
                } else {
                  //On rédirige l'erreur si le montant est incorrect 
                    $url_redirect = "vw_effectuerVirement.php?bad_mt";
                }
              }
          }
        }
        else{
          // On redirige si le mot de passe est incorrect 
          $url_redirect = "vw_effectuerVirement.php?bad_pwd";
        }
      } else if ($_REQUEST['action'] == 'sendmsg') {
          /* ======== MESSAGE ======== */
          //On remplace les caracteres intérdits pour rien ("")
          addMessage($_REQUEST['to'],$_SESSION["connected_user"]["id_user"],str_replace($car_interdits, "", $_REQUEST['sujet']),str_replace($car_interdits, "", $_REQUEST['corps']));
          $url_redirect = "vw_envoyerMessage.php?msg_ok";

      } else if ($_REQUEST['action'] == 'msglist') {
          /* ======== MESSAGE ======== */
          //On retourne la liste des messages recues d'après l'id de l'utilisateur connecté 
          $_SESSION['messagesRecus'] = findMessagesInbox($_SESSION["connected_user"]["id_user"]);
          $url_redirect = "vw_messagerie.php";

      }  else if ($_REQUEST['action'] == 'nvCmpt') {
        /* ======== NOUVEAU COMPTE======== */
        //On remplace les caractères interdits dans le login et on cherche si ce login existe déjà 
        if(userExistant(str_replace($car_interdits, "", $_REQUEST['login']))){
          //S'il existe, on rédirige vers la page avec son erreur respectif lié au login
            $url_redirect = "vw_nouveauCompte.php?nv_userbad=".$_REQUEST['login'];
        }else{
          //S'il n'existe pas on rédirige en disant qu'il y a caractères inderdits dans le login
            $verif=true;
            foreach ($car_interdits as $car) {
              if(strpos($_REQUEST['login'],$car)!==false){
                $url_redirect = "vw_nouveauCompte.php?nv_caraBad";
                $verif=false;
                break;
                }
            }
            if($verif==true){
              //S'il pase la vérification anterieur on valide le mot de passe 
              if(validateMDP($_REQUEST['mdp'])){
                //Si le mot de passe est correct on ajoute l'utilisateur en remplacant les caractères interdits 
                addUser(str_replace($car_interdits, "",$_REQUEST['nom']),str_replace($car_interdits, "",$_REQUEST['prenom']),str_replace($car_interdits, "", $_REQUEST['login']), str_replace($car_interdits, "", $_REQUEST['mdp']));
                //On rédirige à la page de connexiona avec le login pour remplir directement dans le input
                $url_redirect = "vw_login.php?user_create=".$_REQUEST['login'];
              }else{
                //S'il ne passe la validation du mot de passe, on rédirige avec l'erreur 
                $url_redirect = "vw_nouveauCompte.php?nv_passInsecure";
              }
            }
        }
    }else if($_REQUEST['action'] == 'clients'){
        /* ======== FICHES CLIENTS ======== */
        //On obtient la liste des utilisateurs avec un profil client et on rédirige ver la page des Fiches de Clients
        $_SESSION['clients'] = findClients();
        $url_redirect = "vw_fichesClients.php";
    }
  }

  header("Location: $url_redirect");
  exit()

?>
