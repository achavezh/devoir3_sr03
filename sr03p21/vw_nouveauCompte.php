<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script text="text/javascript">

  tbimage=new Array(1,2,3,4,5,6,7,8,9,0)
//Fonction init() : Pour initiliser le pave virtuel
  function init(){
  var tdcollection=document.getElementById('virtualPave').getElementsByTagName('td')

  for (var i = 0; i< tdcollection.length-2; i++){

    if(tbimage.length==1){
      tdcollection[i].firstChild.nodeValue=tbimage[0]
  }
  else{
    var spl=Math.round(Math.random()*(tbimage.length-1))
    tdcollection[i].firstChild.nodeValue=tbimage[spl]
    tbimage.splice(spl,1)
  }
  }

  for (i=0;i<tdcollection.length;i++){
  		tdcollection[i].indice=i
  		tdcollection[i].onclick=function(){if(this.indice<10){document.getElementById('pass').value+=this.innerHTML;}
  		                                                       if(this.indice==10){document.getElementById('pass').value=document.getElementById('pass').value.substr(0,document.getElementById('pass').value.length-1)}
  		                                                       if(this.indice==11){document.getElementById('pass').value=""}
  		                                  }

  		}

}
//Fonction handleClick(cb) : Pour changer le type de l'input qui contient le mot de passe (text vers password et à l'inverse)

  function handleClick(cb) {
    if(cb.checked)
      $('#pass').attr("type","text");
    else
      $('#pass').attr("type","password");
  }

</script>
<title>Nouveau compte</title>
<link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body onload='init()'>

  <div class="nv-compte">
      <h1>Nouveau Compte</h1><hr><br>
          <form method="POST" action="myController.php">
              <input type="hidden" name="action" value="nvCmpt">
              <input type="text" name="prenom" autocomplete="off" required placeholder="Prenom"/>
              <input type="text" name="nom" autocomplete="off" required placeholder="Nom de famille"/>
              <input type="text" name="login" required autocomplete="off" placeholder="Utilisateur"/>
              <?php
              if (isset($_REQUEST["nv_caraBad"])){
                echo '<p> Le login ne doit pas contenir de caractères spéciaux: \',\,;,%,<,>,"\/","\"" </p>';
              }
              ?>
              <input  type='password' id='pass' name='mdp'  autocomplete="off" minlength="8" maxlenght="8" required placeholder="Mot de passe" onkeypress="return false;" autocomplete="off"/><br/>
              <?php
              if (isset($_REQUEST["nv_passInsecure"])){
                echo '<p> Le mot de passe doit avoir 8 caracteres et ne peut être composée des numeros consécutifs (<b>Ex: 12345678</b>)</b><br>
                Il ne doit contenir que des numeros alternés (<b>Ex : 23128212</b>)
                </p>';
              }
              ?>
              <input type="checkbox" onclick="handleClick(this);"/>Montrer/Cacher la mot de passe<br><br>
              <table id='virtualPave' class="login">
              <tr>
              	<td>7</td>
              	<td>8</td>
              	<td>9</td>
              </tr>
              <tr>
              	<td>4</td>
              	<td>5</td>
              	<td>6</td>
              </tr>
              <tr>
              	<td>1</td>
              	<td>2</td>
              	<td>3</td>
              </tr>
              <tr>
              	<td>0</td>
              	<td>e</td>
              	<td>a</td>
              </tr>
            </table><br>
              <button>Créér compte</button>
            </form>
            <?php
              if (isset($_REQUEST["nv_userbad"])) {
                echo '<p>L utilisateur : <b>'.$_REQUEST["nv_userbad"].'</b> existe déjà </p>';
              }
            ?>
    </div>

  <footer>
      <p><b>Authors : </b> Andrea Chávez et Emilien Carrez <b> SR03 P21 </b</p>
  </footer>
</body>
</html>
