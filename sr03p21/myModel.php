<?php
require_once('config/config.php');
require_once('include.php');


/* getMySqliConnection() : Fonction qui retourne la connexion à la BDD */
function getMySqliConnection() {
  return new mysqli(DB_HOST, DB_USER, DB_PASSWD,DB_NAME);
}

/* findUserByLoginPwd(login, pwd, ip) : Fonction qui retourne un array contenant l'information de l'utilisateur trouvé
  il prend son login et son mot de passe pour faire la recherche.
  S'il fait des erreurs sur ses identifiants, on met son ip dans un tableau pour la banner après plusieurs tentatives */
function findUserByLoginPwd($login, $pwd, $ip) {
  $mysqli = getMySqliConnection();

  // On vérifie les érreurs liés à la connexion à la bdd
  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
      $utilisateur = false;
  } else {
      //On commence à préparer la requête pour chercher d'après le login et on vérifie qu'il soit bien préparé :
      if (!($stmt = $mysqli->prepare("SELECT * FROM users WHERE login=?"))) {
        //Requête mal préparé :
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        $utilisateur=false;
      }
      else{//Requête bien préparé :
        $stmt->bind_param("s", $login); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) { //On vérifie aussi la bon exécution de la requête :
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          $utilisateur=false;
        }
        else{
          $stmt->bind_result($id_user, $login,$mot_de_passe, $nom, $prenom,$numero_compte,$profil_user,$solde_compte); // on prépare les variables qui recevront le résultat
          //On vérifie le hash du mot de passe avec password_verify
          if ($stmt->fetch() && password_verify($pwd, $mot_de_passe)) {
            // les identifiants sont corrects => on renvoie les infos de l'utilisateur
            $utilisateur = array ("nom" => $nom,
                                  "prenom" => $prenom,
                                  "login" => $login,
                                  "id_user" => $id_user,
                                  "numero_compte" => $numero_compte,
                                  "profil_user" => $profil_user,
                                  "solde_compte" => $solde_compte);
        }
        else{
          // Identifiants incorrects, alors on insère son IP :
          $stmt->close();
          $utilisateur=false;
          // On prepare le requête :
          if (!($stmt_insert = $mysqli->prepare("insert into connection_errors(ip,error_date) values(?,NOW())"))) {
            echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
          }
          else{
            //S'il est valide on va éxécuter et alors insèrer son IP dans le tableau.
            $stmt_insert->bind_param("s", $ip); 
            // Eventuellement, gérer le cas où l'utilisateur on est derrière un proxy en utilisant $_SERVER['HTTP_X_FORWARDED_FOR']
            if (!$stmt_insert->execute()) {
              echo "Échec lors de l'exécution : (" . $stmt_insert->errno . ") " . $stmt_insert->error;
            }
         }
         $stmt_insert->close();
        }
    }
  }
  $stmt->close();
}
$mysqli->close();

  return $utilisateur;
}

/* verifyMDP(login, mdp) : Fonction qui retourne true si à partir de son login et son mot de passe, trouve un utilisateur */
function verifyMDP($login, $mdp) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
      return false;
  } else {
      if (!($stmt = $mysqli->prepare("SELECT mot_de_passe FROM users WHERE login=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("s", $login); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          $stmt->bind_result($mot_de_passe); // on prépare les variables qui recevront le résultat
          if ($stmt->fetch() && password_verify($mdp, $mot_de_passe)) {
            // les identifiants sont corrects => on renvoie les infos de l'utilisateur
            return true;
          }
    }
  }
  $stmt->close();
}
$mysqli->close();

  return false;
}
/* ipIsBanned(ip) : Fonction qui retourne true si le numéro d'un IP gardé est supérieur à 4, false sinon*/
function ipIsBanned($ip) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
      return false;
  } else {
    // On prepare la requête en utilisant COUNT(*) pour obtenir le nb de tentatives de l'IP recue :
      if (!($stmt = $mysqli->prepare("SELECT COUNT(*) as nb_tentatives from connection_errors where ip=? AND error_date>=DATE_SUB(NOW(), INTERVAL 30 SECOND) AND error_date<=NOW();"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("s",  $ip);
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          $stmt->bind_result($count);
          $stmt->fetch();
          if($count > 4) {
            return true; // cette IP a atteint le nombre maxi de 5 tentatives infructueuses
          } else {
            return false; // L'IP est valide encore
          }
      }
    }
    $stmt->close();

  }
  $mysqli->close();
}

/* findAllUsers() : Fonction qui retourne une liste avec tous les utilisateurs gardés dans la base de données */

function findAllUsers() {
  $mysqli = getMySqliConnection();

  $listeUsers = array();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
  } else {
      $req="SELECT nom, prenom, login, id_user, profil_user FROM users";
      if (!$result = $mysqli->query($req)) {
          trigger_error('Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error, E_USER_ERROR);
      } else {
        // On utilise une boucle while pour trouver tous les lignes trouvés de la table :
          while ($unUser = $result->fetch_assoc()) {
            // On met comme identifiant l'id de l'utilisateur et tous ses informations liés à ce id :
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}
/* tansfert(dest, src, mt) : Fonction fait une update d'un transfert d'argent, ajoute le montant au compte destinaire
  et diminue le montant à la compte source */

function transfert($dest, $src, $mt) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
    trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
    return;
  }else {
      //Update de la compte destinaire 
      if (!($stmt = $mysqli->prepare("UPDATE users SET solde_compte=solde_compte+? WHERE numero_compte=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return;
      }
      else{
        $stmt->bind_param("ss", $mt, $dest);
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return;
        }
        else{
        //Update de la compte source 
          if (!($stmt1 = $mysqli->prepare("UPDATE users SET solde_compte=solde_compte-? WHERE numero_compte=?"))) {
            echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return;
          }
          else{
            $stmt1->bind_param("ss", $mt, $src);
            if (!$stmt1->execute()) {
              echo "Échec lors de l'exécution : (" . $stmt1->errno . ") " . $stmt1->error;
              return ;
            }
          }
          $stmt1->close();
        }
      }
      $stmt->close();
  }
  $mysqli->close();


}
/* compteExistant(dest) : Fonction qui retourne true si un numéro de compte existe dans la base de données, false sinon */

function compteExistant($dest){
  $mysqli = getMySqliConnection();
  if ($mysqli->connect_error) {
    trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
    return false;
  }
  else{
    if (!($stmt = $mysqli->prepare("SELECT * FROM users WHERE numero_compte=?"))) {
      echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
      return false;
    }
    else{
      $stmt->bind_param("s", $dest);
      if (!$stmt->execute()) {
        echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
        return false;
      }
      else{
        if(!$stmt->fetch()) return true;
        else return false;
      }
    }
    $stmt->close();
  }
  $mysqli->close();

}
/* findMessagesInbox(userid) : Fonction qui retourne une liste de listes, chaque composant de la liste contient un liste avec l'information
respectif d'un message */

function findMessagesInbox($userid) {
  $mysqli = getMySqliConnection();
  $listeMessages = array();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
  } else {
      if (!($stmt = $mysqli->prepare("SELECT id_msg,sujet_msg,corps_msg,u.nom,u.prenom FROM messages m, users u WHERE m.id_user_from=u.id_user AND id_user_to=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("i", $userid); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          $stmt->bind_result($id_msg,$sujet_msg,$corps_msg,$nom,$prenom); // on prépare les variables qui recevront le résultat
          while ($stmt->fetch()) {
              // On mer l'information des messages trouvés 
              $unMessage = array ("id_msg" => $id_msg, "sujet_msg" => $sujet_msg, "corps_msg" => $corps_msg, "nom" => $nom, "prenom" => $prenom);
              // On met dans une liste l'array précédent avec l'identifiant du message
              $listeMessages[$id_msg] = $unMessage;
          }

        }
      }
      $stmt->close();
  }
  $mysqli->close();
  return $listeMessages;
}

/* addMessage(to, from, subject, body) : Fonction qui garde un message, il recoit l'expediteur, le destinare, le motif et le corps du message */

function addMessage($to,$from,$subject,$body) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
  } else {
      if (!($stmt = $mysqli->prepare("INSERT INTO messages(id_user_to, id_user_from, sujet_msg, corps_msg) VALUES(?,?,?,?)"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return;
      }
      else{
        $stmt->bind_param("iiss", $to,$from,$subject,$body);
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return;
        }

      }
      $stmt->close();

  }
  $mysqli->close();
}
/* addUser() : Fonction qui ajout un utilisateur dans la base de données */

function addUser($nom, $prenom, $user, $mdp){
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    if (!($stmt = $mysqli->prepare("INSERT INTO users(login, mot_de_passe, nom, prenom, numero_compte, profil_user, solde_compte)
    values(?, ?, ?, ?, ?, ?, ?)"))) {
      echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
      return;
    }
    else{
      $nb_compte = genNumCompte();// On genère un numéro de compte aleatoire 
      $profil = 'CLIENT'; //Les utilisateurs qui se registrent d'après la page nouveauCompte sont de type CLIENT par défait 
      $solde_init = 100; // On met aux utilisateurs un solde début de 100 
      $hash = password_hash($mdp, PASSWORD_DEFAULT); //On fait le hash de son mot de passée avant de la mettre dans la bdd
      $stmt->bind_param("ssssssi", $user,$hash,$nom, $prenom, $nb_compte, $profil, $solde_init); // on lie les paramètres de la requête préparée avec les variables
      if (!$stmt->execute()) {
        echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
        return;
      }
    }
    $stmt->close();
  }
  $mysqli->close();
}
/* userExistant(user) : Fonction qui retourne true si un utilisateur existe déjà d'aprè son login, false sinon*/

function userExistant($user){
  $mysqli = getMySqliConnection();

  $listeMessages = array();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
      return false;
  } else {
      if (!($stmt = $mysqli->prepare("SELECT * FROM users WHERE login=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("s", $user); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          if($stmt->fetch()) return true;
          else return false;
        }
      }
      $stmt->close();
  }
  $mysqli->close();
  return $listeMessages;
}

/* numAllCompter() : Fonction qui retourne une liste avec tous les numéro de comptes gardés dans la base de données */

function numAllComptes() {
  $mysqli = getMySqliConnection();

  $listeComptes = array();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
  } else {
      $req="SELECT numero_compte FROM users";
      if (!$result = $mysqli->query($req)) {
          trigger_error('Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error, E_USER_ERROR);
      } else {
          $i=0;
          while ($unCompte = $result->fetch_assoc()) {
            $listeComptes[$i]= $unCompte['numero_compte'];
            $i=$i+1;

          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeComptes;
}

/* genNumCompte() : Fonction qui retourne une numéro de compte aléaotire de longueur 12 */


function genNumCompte(){

  $carac = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';//Caractères qui porront être utilisées dans le numéro de compte 
	$longueur_caracteres = strlen($carac);
  $nums_comptes=numAllComptes();
  $verif = false;
  while($verif == false){
    $verif = true;
    $chaine_random = '';
    for ($c = 0; $c < 12; $c++) {//Cette boucle est dédié à composer le numéro de compte
        $chaine_random .= $carac[random_int(0, $longueur_caracteres - 1)];
    }
    foreach($nums_comptes as $num){ //Cette boucle est dédié à vérifier que le numéro de compte généré précedement n'existe pas 
      if($num == $chaine_random){
        $verif=false;
      }
    }
  }
	return $chaine_random;
}
/* findClients() : Fonction qui retourne une liste de listes, chaque composant de la liste aura une liste avec l'informations des
utilisateurs de profil CLIENT gardés dans la base de données */

function findClients() {
  $mysqli = getMySqliConnection();
  $listeUsers = array();
  $profil = "CLIENT";
  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
  } else {
      if (!($stmt = $mysqli->prepare("SELECT id_user, nom, prenom, numero_compte, solde_compte FROM users WHERE profil_user=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("s", $profil); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          $stmt->bind_result($id_user,$nom,$prenom, $numero_compte, $solde_compte); // on prépare les variables qui recevront le résultat
          while ($stmt->fetch()) {
            // On crée un array contenant l'information de l'utilisateur 
              $user = array ("id_user" => $id_user, "nom" => $nom, "prenom" => $prenom, "numero_compte" => $numero_compte, "solde_compte" => $solde_compte);
            //On met l'array précédent dans une liste qui aura chaque utilisateur identifié avec son id 
              $listeUsers[$id_user] = $user;
          }

        }
      }
      $stmt->close();
  }
  $mysqli->close();
  return $listeUsers;
}
/* validateMDP(mdp) : Fonction qui retourne true si une mot de passe recue est valide : 
* longuer : 8 caractères
* numéros pas consécutifs
* numéros pas répétés l'un après l'autre

Il retourne false sinon*/

function validateMDP($mdp){
  $nb_num_consecutifs = 0;
  if(strlen($mdp)==8){
    for($i = 0; $i<strlen($mdp)-1; $i++){
      if($mdp[$i+1] == $mdp[$i]) {
        return false;
      }if($mdp[$i+1] == $mdp[$i]+1){
        $nb_num_consecutifs += 1;
        if($nb_num_consecutifs > 4) return false;
      }
    }
  }else{
    return false;
  }
  return true;
}
/* findSolde(cmp) : Fonction qui retourne le solde d'après le paramètre qui est le numéro de compte et false si le numero de compte est null */

function findSolde($cmp) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      trigger_error('Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error, E_USER_ERROR);
      return false;
  } else {
      if (!($stmt = $mysqli->prepare("SELECT solde_compte FROM users WHERE numero_compte=?"))) {
        echo "Échec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
        return false;
      }
      else{
        $stmt->bind_param("s", $cmp); // on lie les paramètres de la requête préparée avec les variables
        if (!$stmt->execute()) {
          echo "Échec lors de l'exécution : (" . $stmt->errno . ") " . $stmt->error;
          return false;
        }
        else{
          $stmt->bind_result($solde_compte); // on prépare les variables qui recevront le résultat
          if ($stmt->fetch()) {
            // les identifiants sont corrects => on renvoie les infos de l'utilisateur
            $solde=$solde_compte;
        }
        else{
          return false;
        }
    }
  }
  $stmt->close();
}
$mysqli->close();

  return $solde;
}

?>
