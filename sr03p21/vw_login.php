<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script text="text/javascript">

tbimage=new Array(1,2,3,4,5,6,7,8,9,0)//Numéros qui peut contenir le pave Virtuel
//Fonction init() : Pour initiliser le pave virtuel

function init(){
  var tdcollection=document.getElementById('virtualPave').getElementsByTagName('td')

  for (var i = 0; i< tdcollection.length-2; i++){

    if(tbimage.length==1){
      tdcollection[i].firstChild.nodeValue=tbimage[0]
  }
  else{
    var spl=Math.round(Math.random()*(tbimage.length-1))
    tdcollection[i].firstChild.nodeValue=tbimage[spl]
    tbimage.splice(spl,1)
  }
  }

  for (i=0;i<tdcollection.length;i++){
      tdcollection[i].indice=i
      tdcollection[i].onclick=function(){if(this.indice<10){document.getElementById('pass').value+=this.innerHTML;}
          if(this.indice==10){document.getElementById('pass').value=document.getElementById('pass').value.substr(0,document.getElementById('pass').value.length-1)}
          if(this.indice==11){document.getElementById('pass').value=""}
          }

      }
    }

//Fonction handleClick(cb) : Pour changer le type de l'input qui contient le mot de passe (text vers password et à l'inverse)

  function handleClick(cb) {
    if(cb.checked)
      $('#pass').attr("type","text");
    else
      $('#pass').attr("type","password");
  }
</script>
  <title>Connexion</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body onload='init()'>

  <div class="login-page">
      <h1>Connexion</h1><hr><br>
          <form method="POST" action="myController.php">
              <input type="hidden" name="action" value="authenticate">
              <?php
              if (!isset($_REQUEST["user_create"])) {
               ?>
              <input type="text" name="login" autocomplete="off" required placeholder="Utilisateur"/>
              <?php
              }else{
              ?>
              <input type="text" name="login" value=<?php echo $_REQUEST["user_create"]?> required autocomplete="off" placeholder="Utilisateur"/>
              <?php
              } ?>
              <input type='password' id='pass' name='mdp' minlength="8" maxlength="8" required  autocomplete="off" placeholder="Mot de passe" onkeypress="return false;" autocomplete="off"/><br/>
              <input type="checkbox" onclick="handleClick(this);"/>Montrer/Cacher la mot de passe<br><br>
              <div class="authentificate">
                <table id='virtualPave' class="login">
                <tr>
                  <td>7</td>
                  <td>8</td>
                  <td>9</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                </tr>
                <tr>
                  <td>0</td>
                  <td>a</td>
                  <td>e</td>
                </tr>
              </table><br>
              <div class="g-recaptcha" data-sitekey="6LfEmu0aAAAAAMMqh84mtChRPbZdWM8Jh9ra5oCi"></div>
            </div><br><br>
            <button>Connexion</button><br><br>
            </form>
            <a href="vw_nouveauCompte.php">Créer un nouveau compte</a>
            <?php
              if (isset($_REQUEST["nullvalue"])) {
                echo '<p class="errmsg">Merci de saisir votre login et votre mot de passe</p>';
              } else if (isset($_REQUEST["badvalue"])) {
                echo '<p class="errmsg">Votre login/mot de passe est incorrect</p>';
              } else if (isset($_REQUEST["disconnect"])) {
                echo '<p>Vous avez bien été déconnecté.</p>';
              }
              else if (isset($_REQUEST["ipbanned"])) {
                echo '<p class="errmsg"> Nombre de tentatives maximal atteinte ! Contactez votre gestionnaire.</p>';
              }
              else if (isset($_REQUEST["badCaptcha"])){
                echo '<p class="errmsg"> Captcha invalide </p>';

              }
              else if (isset($_REQUEST["forgotCaptcha"])){
                echo '<p class="errmsg"> Veuillez cocher "je ne suis pas un robot" </p>';

              }
            ?>
    </div>


  <footer>
      <p><b>Authors : </b> Andrea Chávez et Emilien Carrez <b> SR03 P21 </b</p>
  </footer>

</body>
</html>
