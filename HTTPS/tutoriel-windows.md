#   Tutoriel de mise en place d'HTTPS sous wamp64

## Placer le certificat serveur ainsi que sa clé privée
    Se déplacer dans D:\wamp64\bin\apache\apache2.4.46\conf
    Crée un dossier certs et private
    Placer dans certs le fichier ServSR03.crt (certificat serveur) ainsi que SACA-CC.pem (chaine de confiance du certificat serveur permettant au client de vérifier le certificat serveur)
    Placer dans private la clé ServSR03.pem

## Importer le certificat racine dans firefox

    Importer dans firefox le certificat racine de la chaine de confiance RCA.crt afin de compléter le processus de vérification du certificat serveur (pour cela aller dans options/Vie privée et sécurité/Afficher les certificats.../Autoriés/Importer/RCA.crt)

## Modification des fichiers de configuration du serveur
### Modification du fichier httpd.conf (D:\wamp64\bin\apache\apache2.4.46\conf\httpd.conf)

    Dé-commenter la ligne 'LoadModule ssl_module modules/mod_ssl.so' et la ligne 'Include conf/extra/httpd-ssl.conf'
    Dé-commenter la ligne LoadModule socache_shmcb_module modules/mod_socache_shmcb.so

 ### Modification du fichier php.ini (D:\wamp64\bin\php\php7.3.21\php.ini)  

    Dé-commenter la ligne extension=php_openssl.dll

### Modification du fichier httpd-ssl.conf (D:\wamp64\bin\apache\apache2.4.46\conf\extra\httpd-ssl.conf) 

    Rechercher la ligne <VirtualHost_default:443>
    Sous cette ligne remplacer la ligne 'DocumentRoot...' par DocumentRoot "D:/wamp64/www/sr03p21/" (chemin vers le site)
    Remplacer ServerName... par ServerName localhost  
    Remplacer ErrorLog... par ErrorLog "D:/wamp64/bin/apache/apache2.4.46/logs/ssl_error.log"
    Remplacer TransferLog par TransferLog "D:/wamp64/bin/apache/apache2.4.46/logs/ssl_access.log"
    Remplacer SSLCertificateFile... par SSLCertificateFile "D:/wamp64/bin/apache/apache2.4.46/conf/certs/ServSR03.crt"
    Remplacer SSLCertificateKey... par SSLCertificateKeyFile "D:/wamp64/bin/apache/apache2.4.46/conf/private/ServSR03.pem"
    Remplacer <Directory...> par <Directory "D:/wamp64/www/sr03p21">
    Supprimer la ligne CustomLog...

**RELANCER LE SERVEUR WAMP !!**


    
